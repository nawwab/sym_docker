# Symbolic Docker 0.1

This service are needed if you want to run all database and dependencies on docker

# Prerequisite
1. clone all projects and dependencies with this structure
```
root
symbolic-admin
symbolic-api
symbolic-docker
plugin 
 | sym_auth
 | ...
 | sym_zettelkasten
```

2. create `.env` file (you can refer to `.env.example`)
3. install docker-desktop
3. run `docker compose up`

<!---
TODO: fix this commands list below
-->
# Commands
```
# edit credentials
# (please refer to /config/credentials/development.docker.yml.example)
rails credentials:edit --environment development

# start admin service
docker compose --env-file .env.docker up
# start db services
docker compose -f docker-compose.db.yml up

# rails s not started by default, use terminal below
# use terminal on admin service
docker compose --env-file .env.docker exec admin /bin/sh
# use terminal on other services
docker compose --env-file .env.docker exec <other services> /bin/sh
```
